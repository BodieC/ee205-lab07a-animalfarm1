///////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - animalFarm1 - EE 205 - Spr 2022
///
/// @file catDatabse.h
///
///Database for Cats
///
///@author Bodie Collins <bodie@hawaii.edu>
/// @date 22 Feb 2022
/////////////////////////////////////////////////////////////////////////////
#pragma once
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

extern const size_t MAXNAMELENGTH;
#define MAXCATSINDATABASE 5

// name char array
extern char* nameArray[MAXCATSINDATABASE];
// enums of genders
enum gender {UNKNOWN_GENDER, MALE, FEMALE};
extern enum gender genderArray[MAXCATSINDATABASE];
// enums of breed
enum breed{UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
extern enum breed breedArray[MAXCATSINDATABASE];
// is fixed bool
extern bool fixedArray[MAXCATSINDATABASE];
// weight float
extern float weightArray[MAXCATSINDATABASE+1];
// number of cats
extern size_t numofCats;

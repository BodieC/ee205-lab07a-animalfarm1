////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
///
/// Usage:  To store,update, print , and delete cats into a database
///
/// Result:
///   Print out the energy unit conversion
///
/// Compilation:
///   $ make animalFarm
///
/// @file animalFarm1.c
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  20 FEB 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "catDatabse.h" 
#include "addCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"
#include "config.h"
int main()
{
printf("Starting Animal Farm 1\n");

addCats( "Loki", MALE, PERSIAN, true, 8.5 ) ;
addCats( "Milo", MALE, MANX, true, 30.0 ) ;
addCats( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
addCats( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
addCats( "Bell", FEMALE, MANX, true, 12.2 ) ;
addCats( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
//printAllCats();
//printCat(2);
//printCat(6);
//printCat(0);
//printCat(35);
//findCat ("Milo");
//findCat ("Bell");
//findCat ("Dog");
//findCat("Loki");

//printCat(2);
//updateCatName(2 , "Loki");
//printCat(2);

//printCat(3);
//fixCat(3);
//printCat(3);

//printCat(5);
//updateCatWeight(5,0);
//printCat(5);

//printAllCats();
//deleteAllCats();
//printAllCats();

//printAllCats();
//deleteCat(3);
//printAllCats();

printf("Ending Animal Farm 1\n");
}


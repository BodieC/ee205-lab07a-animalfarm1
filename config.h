/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - animalFarm1 - EE 205 - Spr 2022
///
/// @file config.h
///
/// @author Bodie Collins <bodie@hawaii.edu>
/// @date 22 Feb 2022
/////////////////////////////////////////////////////////////////////////////
#pragma once
#define ADD_CATS "Add Cats"
#define REPORT_CATS "Report Cats"
#define UPDATE_CATS "Update Cats"

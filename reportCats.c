
////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
///
/// @file printCats.c
/// @version 1.0
///
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  22 FEB 2022
///////////////////////////////////////////////////////////////////////
#include <stddef.h>
#include "reportCats.h"
#include "config.h"

//FUNCTION TO PRINT ALL CATS
int printAllCats()
{
   //temp variable to cylce through printing names
   //since addCats increments varible after inputing data last cat does have any data in in yet
   //so use numofCats -1
   int ii=numofCats-1;
   while( ii >= 0)
   {
      printf ("-----------------------------------");
      printf ("Cat number %d information\n",ii);
      printf ("name= %s\n",nameArray[ii]);
      printf ("gender= %d\n", genderArray[ii]);
      printf ("breed=%d\n",breedArray[ii]);
      printf ("is fixed= %d\n",fixedArray[ii]);
      printf ("weight =%f\n",weightArray[ii]);

      ii=ii-1;

   }
return 1;
}

//FUNCTION TO PRINT CAT BY NUMBER IN ARRAY
int printCat(size_t catnuM)
{
   if ( catnuM > numofCats-1)
   {
      fprintf( stderr, "%s: Cat [%lu] not found, Badcat.\n", REPORT_CATS, catnuM );
      return 0;
   }
   else
   {
      printf ("-----------------------------------");
      printf ("Cat number %lu information\n",catnuM);
      printf ("name= %s\n",nameArray[catnuM]);
      printf ("gender= %d\n", genderArray[catnuM]);
      printf ("breed=%d\n",breedArray[catnuM]);
      printf ("is fixed= %d\n",fixedArray[catnuM]);
      printf ("weight =%f\n",weightArray[catnuM]);
      return 1;
   }
}
//FUNCTION TO FIND A CAT BY NAME
int findCat(char* naME)
{
   int iii=numofCats;
   while(iii>=0)
   {
      //if name matches cat number iii
      if (naME ==nameArray[iii])
         {
         printf ("-----------------------------------");
         printf ("Cat number %d information\n",iii);
         printf ("name= %s\n",nameArray[iii]);
         printf ("gender= %d\n", genderArray[iii]);
         printf ("breed=%d\n",breedArray[iii]);
         printf ("is fixed= %d\n",fixedArray[iii]);
         printf ("weight =%f\n",weightArray[iii]);
         break;
         }
      //if cat name doesnt match iii decrement to check next name
      else if(naME !=nameArray[iii])
      {
         iii=iii-1;

         // if its last name print error
         if (iii==0)
         {
            fprintf( stderr, "%s:Cat name [%s] not found\n" , REPORT_CATS, naME ) ;
            break;
         }

         continue;
      }

   }

   return iii;
}


////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
///
/// @file updateCats.h
/// @version 1.0
///
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  22 FEB 2022
///////////////////////////////////////////////////////////////////////
#pragma once
#include "catDatabse.h"
extern int updateCatName(size_t catnUM, char* nAME);
extern int fixCat(size_t catNUM);
extern int updateCatWeight(size_t caTNUM, float weigHT);


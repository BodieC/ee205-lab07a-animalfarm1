////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  22 FEB 2022
///////////////////////////////////////////////////////////////////////

#include "catDatabse.h"
#include "updateCats.h"
#include "config.h"

int updateCatName(size_t catnUM, char* nAME)
{
   int erroR=0;
   //check if name is empty erroR=true if empty
   if (strlen(nAME)==0)
   {
      //if name is empty error
      fprintf( stderr, "%s: You did not enter a name\n", UPDATE_CATS) ;
      erroR=1;
   }

   else if (strlen(nAME) > MAXNAMELENGTH)
   {
      //if name is to long error
      fprintf( stderr, "%s: Cat name [%s] is to long\n",UPDATE_CATS, nAME);
      erroR=1;
   }

   //run through list of name change erroR to true if same name is already there
   size_t iiii=numofCats;
   while(iiii >  0)
      {

            // if the name is the same as the name before it
            if(nAME == nameArray[iiii-1])
               {

               //print error
               fprintf( stderr, "%s: Cat name [%s] is already in the database.\n", UPDATE_CATS, nAME) ;
               erroR=1;
               break;
               }
            else if (nAME != nameArray[iiii-1])
            {
               // iterate varible to check for next name
               #ifdef DEBUG
               printf("current name %s vs name -1 %s\n", namE, nameArray[iiii-1]);
               #endif

               iiii--;
               continue;
            }
      }
   // if passes validation update name
   if(erroR==0)
   {
   nameArray[catnUM]=nAME;
   }
if (erroR ==0)
{
   return 1;
}
else 
{
   return 0;
}
}

//FUNCTION TO CHANGE IF A CAT IS FIXED
int fixCat(size_t catNUM)
{
   fixedArray[catNUM]=1;
   return 1;
}

//FUNCTION TO UPDATE CATS WEIGHT
int updateCatWeight(size_t caTNUM, float weigHT)
{
   if(weigHT>0)
   {
      weightArray[caTNUM]= weigHT;
      return 1;
   }
   else
   {
      fprintf( stderr, "%s: Weight Cannot be equal or less than zero\n", UPDATE_CATS) ;
      return 0;
   }
}


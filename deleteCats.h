/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - animalFarm1 - EE 205 - Spr 2022
///
/// @file deleteCats.h
///
/// @author Bodie Collins <bodie@hawaii.edu>
/// @date 22 Feb 2022
/////////////////////////////////////////////////////////////////////////////
#pragma once
#include "catDatabse.h"

extern int deleteAllCats();
extern int deleteCat(size_t cATNUM);


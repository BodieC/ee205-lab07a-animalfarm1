////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  22 FEB 2022
///////////////////////////////////////////////////////////////////////
#include "catDatabse.h"
#include "deleteCats.h"
#include "config.h"

//FUNCTION TO DELETE ALL CATS
int deleteAllCats()
{
   size_t foo=numofCats;

   while(foo>0)
   {
   nameArray[foo]="";
   genderArray[foo]=0;
   breedArray[foo]=0;
   fixedArray[foo]=0;
   weightArray[foo]=0;
   foo=foo-1;
   }
   return 1;
}

//DELETE ONE CAT
int deleteCat(size_t cATNUM)
{

   nameArray[cATNUM]="";
   genderArray[cATNUM]=0;
   breedArray[cATNUM]=0;
   fixedArray[cATNUM]=0;
   weightArray[cATNUM]=0;
   return 1;
}

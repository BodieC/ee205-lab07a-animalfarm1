///////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - animalFarm1 - EE 205 - Spr 2022
///
/// @file catDatabse.c
///
///Database for Cats
///
///@author Bodie Collins <bodie@hawaii.edu>
/// @date 22 Feb 2022
/////////////////////////////////////////////////////////////////////////////
#include "catDatabse.h"
#include "config.h"

const size_t MAXNAMELENGTH=30;

char* nameArray[MAXCATSINDATABASE];
enum gender genderArray[MAXCATSINDATABASE];
enum breed breedArray[MAXCATSINDATABASE];
bool fixedArray[MAXCATSINDATABASE];
float weightArray[MAXCATSINDATABASE+1];

size_t numofCats=0;


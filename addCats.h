/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - animalFarm1 - EE 205 - Spr 2022
///
/// @file addCats.h
///
/// @author Bodie Collins <bodie@hawaii.edu>
/// @date 22 Feb 2022
/////////////////////////////////////////////////////////////////////////////
#pragma once
#include <stdbool.h>
extern int addCats(char* namE,int gendeR, int breeD, bool fixeD, float weighT ) ;


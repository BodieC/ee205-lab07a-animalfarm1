###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 07d - animalFarm1 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### animalFarm1
###
### @author Bodie Collins <bodie@hawaii.edu>
### @date 22 Feb 2022
###
###############################################################################
CC = gcc
CFLAGS = -g -Wall -Wextra

TARGET =animalFarm1

all: $(TARGET) 

catDatabse.o: catDatabse.c catDatabse.h config.h
	$(CC) $(CFLAGS) -c catDatabse.c

addCats.o: addCats.c addCats.h catDatabse.h config.h
	$(CC) $(CFLAGS) -c addCats.c

reportCats.o: reportCats.c reportCats.h catDatabse.h config.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h catDatabse.h config.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h catDatabse.h config.h
	$(CC) $(CFLAGS) -c deleteCats.c

animalFarm1.o: animalFarm1.c catDatabse.h addCats.h config.h
	$(CC) $(CFLAGS) -c animalFarm1.c


animalFarm1: animalFarm1.o addCats.o catDatabse.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET) animalFarm1.o addCats.o catDatabse.o reportCats.o updateCats.o deleteCats.o


clean:
	rm -f $(TARGET) animalFarm1.o addCats.o catDatabse.o reportCats.o  updateCats.o deleteCats.o *.o


////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  22 FEB 2022
///////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "addCats.h"
#include "config.h"
#include "catDatabse.h"

int addCats(char * namE, int gendeR, int breeD, bool fixeD, float weighT)
{
   int error=0;

   if (numofCats>MAXCATSINDATABASE)
   {
     //if max cats is reached return error
      fprintf( stderr, "%s: Cat Database Full.\n", ADD_CATS);
      error=1;
   }
   else if (strlen(namE) > MAXNAMELENGTH)
   {
      //if name is to long error
      fprintf( stderr, "%s: Cat name [%s] is too long.\n", ADD_CATS , namE );
      error=1;
   }
   else if (strlen(namE)==0)
   {
      //if name is empty error
      fprintf( stderr, "%s: You did not enter a name.\n", ADD_CATS);
      error=1;
   }
   else if (weighT==0)
   {
      //if weight is equal to 0 error
      fprintf( stderr, "%s: Weight Cannot equal 0\n", ADD_CATS);
      error=1;;
 }

   // tmp variable to go through array to check if same name
   size_t i=numofCats;

   //if it passes first set of validation check for same name
   if (error!=1)
   {
      while(i >  0)
      {

            // if the name is the same as the name before it
            if(namE == nameArray[i-1])
               {

               //print error
               fprintf( stderr, "%s: Cat name [%s] is already in the database.\n", ADD_CATS , namE ) ;
               error=1;
               break;
               }
            else if (namE != nameArray[i-1])
            {
               // iterate varible to check for next name
               #ifdef DEBUG
               printf("current name %s vs name -1 %s\n", namE, nameArray[i-1]);
               #endif

               i--;
               continue;
            }


      }
      // if it passes same name check enter into database
      if (error != 1)
      {        
               //Since passed validation enter into database
               nameArray[numofCats]=namE;
               
               genderArray[numofCats]=gendeR;
               
               breedArray[numofCats]=breeD;
               
               fixedArray[numofCats]=fixeD;
               
               weightArray[numofCats]=weighT;
                
               
               //increment # of cats for next input 
               numofCats=numofCats+1;
            
         //since numofCats+1 has no data yet return last cat with data
         return numofCats-1;

      }
   }
return 1;
}

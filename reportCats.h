////////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalFarm1 - EE 205 - Spr 2022
///
/// @file printCats.h
/// @version 1.0
///
///
/// @author Bodie Collins < bodie@hawaii.edu>
/// @date  22 FEB 2022
///////////////////////////////////////////////////////////////////////
#pragma once
#include "catDatabse.h"
extern int printAllCats();
extern int printCat(size_t catnuM);
extern int findCat(char* naME);

